var express = require('express'),
logger = require('morgan'),
errorhandler = require('errorhandler'),
router = express.Router(),
bodyParser = require('body-parser');

var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

if (!isProduction) {
  app.use(errorhandler());
}

var api_index = require('./routes/api/index.js');
var api_users = require('./routes/api/users.js');
var api_news = require('./routes/api/news.js');

app.use('/api/v1', api_index);
app.use('/api/v1',  api_users);
app.use('/api/v1',  api_news);


/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (!isProduction) {
  app.use(function(err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({'errors': {
      message: err.message,
      error: err
    }});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: {}
  }});
});

module.exports = app;
