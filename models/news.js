"use strict";

module.exports = function(sequelize, DataTypes) {
  var News = sequelize.define("News", {
    title: DataTypes.STRING,
    main_text: DataTypes.STRING
  });

  return News;
};
