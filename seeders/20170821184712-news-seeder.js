'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return [
      queryInterface.bulkInsert('News', [{
        title: "Россия погасила последний внешний долг СССР",
        main_text: "Долг перед Боснией и Герцеговиной возник в связи с тем, что Россия признала ответственность по обязательствам бывшего СССР перед бывшей Югославией. С другими бывшими югославскими республиками (Хорватией, Сербией, Черногорией, Словенией и Македонией) Россия полностью расплатилась в 2011-2016 годах."
      }])
    ];
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('News', null, {});
  }
};
