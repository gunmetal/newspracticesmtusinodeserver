# Set the base image to stable version of node
FROM node:6.11.2

RUN apt-get install mysql-client

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/

# Install nodemon
RUN npm install -g sequelize-cli

RUN pwd
RUN npm install
# CMD npm run start:dev
