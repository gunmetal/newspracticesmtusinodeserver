var models = require('../models/index');

// Display list of all News
exports.news_list = function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    //console.log(models);
    models.News.findAll({limit: 100, order: [['updatedAt', 'DESC']]}).then(function(news) {
       res.json(news);
    });
};

//create news
exports.create_news = function(req, res) {
  models.News.create({
    title: req.body.title,
    main_text: req.body.main_text
  }).then(function() {
    return res.sendStatus(200);
  });
};

exports.remove_news = function(req, res) {

}
