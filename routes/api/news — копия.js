var models = require('../../models');
var express = require('express');
var router = express.Router();

var news_controller = require('../../controllers/newsController.js');

// create news
router.post('/news', news_controller.create_news);

//get news list
router.get('/news', news_controller.news_list);

router.delete('/news/:news_id', news_controller.delete_news);

//get news by id
router.get('/news/:news_id', news_controller.get_news_by_id);

router.put('/news/:news_id', news_controller.edit_news_by_id);


module.exports = router;
