var models = require('../../models');
var express = require('express');
var router = express.Router();

var news_controller = require('../../controllers/newsController.js');

// create news
router.post('/news', news_controller.create_news);

//get news list
router.get('/news', news_controller.news_list);


//delete news
router.delete('/news/:news_id', function(req, res) {
  models.User.destroy({
    where: {
      id: req.params.news_id
    }
  }).then(function() {
    return res.sendStatus(200);
  });
});

module.exports = router;
