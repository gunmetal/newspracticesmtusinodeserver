var models = require('../../models');
var express = require('express');
var router = express.Router();

// create user
router.post('/users', function(req, res) {
    models.User.create({
      username: req.body.username
    }).then(function() {
      //return res.sendStatus(200).send('Status: 200 OK');
      res.json({ message: 'user was created' });
    });
});

//delete user
router.delete('/users/:user_id', function(req, res) {
    res.send('This is not implemented now');
});

//get all users. Limit 200.
router.get('/users', function(req, res) {
    res.send('This is not implemented now');
});

//get information about user
router.get('/users/:user_id', function(req, res) {
    res.send('This is not implemented now');
});

//edit information about user
router.put('/users/:user_id', function(req, res) {
    res.send('This is not implemented now');
});

module.exports = router;
