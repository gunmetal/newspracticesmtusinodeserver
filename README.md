## Docker
```
docker-compose up -d
```
## ручной запуск
```
npm install
npm install -g sequelize-cli
mysql -e "CREATE USER dev@localhost IDENTIFIED BY 'devFrrjfydhe'; GRANT ALL ON *.* TO dev@localhost; FLUSH PRIVILEGES;"
mysql -e 'create database android_app_api_db_dev;'
sequelize db:migrate --env dev
npm run start:dev
```
## Сервер с api 
```
http://91.218.251.70:80/api/v1/
```